// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { store } from './store/store'

import {ApiPost, ApiGet, ApiPut, ApiDelete} from "./helper/api"

import './assets/css/bootstrap.min.css';
import './assets/css/scss/home.scss';
import './assets/css/scss/details.scss';
import './assets/css/scss/list.scss';
import './assets/fontawesome/css/all.css';

Vue.config.productionTip = false

console.log('VUE_APP_API_URL', process.env.VUE_APP_API_URL)
console.log('WEBSITE_URL', process.env.WEBSITE_URL)

window.ApiGet = ApiGet;
window.ApiPost = ApiPost;
window.ApiPut = ApiPut;
window.ApiDelete = ApiDelete;
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
