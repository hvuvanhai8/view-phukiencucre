const home = {
    state: {
        listProductsHot: [],
        loglog: false
    },
    getters: {
        LIST_PRODUCTS_HOT(state) {
            return state.listProductsHot
        }
        ,HAI_LOG(state) {
            return state.loglog
        }
    },
    mutations: {
        SET_LIST_PRODUCTS_HOT: (state, data) => {
            state.listProductsHot = data
        }
    },
    actions: {
        getListProductsHot({ commit }) {
            const params = {
                fields: 'id,name,price_discount,hot,price,image',
                where: 'hot+1'
            }
            ApiGet("/products", params).then(response => {
                console.log('response', response)
                commit('SET_LIST_PRODUCTS_HOT', response.data.data)
            }).catch(error => {
                console.log('error', error)
            })
        },
        getTest() {
            console.log('get test')
        }
    }
}


export default home