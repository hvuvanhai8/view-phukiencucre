const login = {
    state: {
        logout: true
    },
    getters: {
        LOGIN(state){
            return state.logout;
        }
    },
    mutations: {
    },
    actions: {}
}

export default login