import Vue from 'vue'
import Vuex from 'vuex'
import login from './modules/login'
import home from './modules/home' 
import { getField, updateField } from 'vuex-map-fields';

Vue.use(Vuex)

export const store = new Vuex.Store({
    namespace: true,
    modules: {
        home,
        login
    },
    state: {
        listProductsHot: [1]
    },
    getters: { 
    },
    mutations: {
    },
    actions: {
    }
})